package com.automation.ui.base.common.api.adapter;

import com.google.gson.JsonElement;

public interface RestAdapter {

   // JsonElement execute();

    <T> T execute(Class<T> responseClass);
 }