package com.automation.ui.base.common.api.clientimpl.apacheimpl;

import com.automation.ui.base.common.api.util.MethodType;

import com.automation.ui.base.common.logging.Log;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.client.methods.*;

import java.net.*;

import java.util.ArrayList;

public class EmailRegisteration extends ApiCall {

    private ArrayList<BasicNameValuePair> params = new ArrayList<>();

    public static void main(String str[]) throws Exception
    {
        EmailRegisteration ur= new EmailRegisteration();
        String emailCreationStatus= ur.registerUserEmailConfirmed("ddddd111@honeywell.com");
        System.out.println("mail"+emailCreationStatus);
    }


    public EmailRegisteration() {
        super();
    }




    private static HttpGet addHttpGet(URL url) throws URISyntaxException {
        return new HttpGet(new URIBuilder((new URI(url.getProtocol(), url.getUserInfo(),
                url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef())))
                //.addParameter("access_token", SITE_PRODUCTION_APP_ACCESS_TOKEN)
                .build());
    }

    public String registerUserEmailConfirmed(String email) {

        HttpRequestOptions hro = new HttpRequestOptions(getURL(email), MethodType.GET);
        hro.ignoreCert = true;

        try {
            String urlString = new URIBuilder(getURL(email))
                    .setParameter("text", "text")
                    .setParameter("summary", "SUMMARY_QM")
                    .build()
                    .toASCIIString();
        } catch (Exception e) {
            Log.logError("Error during registering user", e);
        }

        return  call(hro);
    }




    protected String getURL(String email) {
        return "https://enabledservicesqa.honeywell.com:443/gateway/AddUserEmailMapping/c026d7fa-d772-4511-ac8d-fbe9465f8702/"+email;
    }
    protected ArrayList<BasicNameValuePair> getParams() {
        // params.add(new BasicNameValuePair("username", user.getUserName()));
        // params.add(new BasicNameValuePair("password", user.getPassword()));
        return params;
    }
}
