package com.automation.ui.base.common.api.clientimpl.apacheimpl;

import com.automation.ui.base.common.api.clientimpl.apacheimpl.secure.NoopTrustManager;
import com.automation.ui.base.common.api.util.MethodType;
import com.automation.ui.base.common.auth.User;
import com.automation.ui.base.common.constants.BASEConstants;
import com.automation.ui.base.common.core.Helios;
import com.automation.ui.base.common.logging.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.WebDriverException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class HttpConfig {

    protected static String url = null;
    private static String ERROR_MESSAGE = "Problem with API call";
    private CloseableHttpClient httpClient = null;
    private MethodType httpVerb;
    private HttpRequestBase httpRequest;
    private HttpResponse response;

    protected HttpConfig() {
    }



    /**
     * Return null when no params should be added to API call
     *
     * @return params
     */
    abstract protected ArrayList<BasicNameValuePair> getParams();



    private void setRequestType(HttpRequestOptions options) {
        this.url = options.url;
        this.httpVerb = options.httpVerb;

        this.httpClient = createHttpClient(options.ignoreCert);
        System.out.println("httpRequest :: " + httpRequest +"::"+this.httpVerb);

        switch (this.httpVerb) {
            case GET:
                httpRequest = new HttpGet(url);
                break;
            case HEAD:
                httpRequest = new HttpHead(url);
                break;
            case OPTIONS:
                httpRequest = new HttpOptions(url);
                break;
            case PATCH:
                httpRequest = new HttpPatch(url);
                break;
            case POST:
                httpRequest = new HttpPost(url);
                break;
            case PUT:
                httpRequest = new HttpPut(url);
                break;
            case DELETE:
                httpRequest = new HttpDelete(url);
                break;
            case DELETE_WITH_BODY:
                httpRequest = new HttpDeleteWithBody(url);
                break;
            default:
                throw new RuntimeException(String.format("HTTP verb \"%s\" is not supported", this.httpVerb));
        }
        System.out.println("httpRequest :: " + httpRequest +"::"+this.httpVerb);


        if (options.proxy != null && !options.proxy.trim().isEmpty()) {
            this.setProxy(options.proxy);
        }
        setHeaders(this.httpRequest);
        //print header
        getRequestHeader(this.httpRequest);
    }

    //Param response.getBody().toString()
    public String prettyPrint(String body) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(body);
        String prettyJsonString = gson.toJson(je);
        // System.out.println(prettyJsonString);
        return gson.toJson(je);
    }


    private void getRequestHeader(HttpRequestBase httpRequest){
        Header hdr[] = httpRequest.getAllHeaders();
        for(int i=0;i<hdr.length ;i++)
        {
            Header key = hdr[i];
            System.out.println("Request Header Name :: " + key.getName() + " Header Value :: "+ key.getValue());
        }
    }

    private CloseableHttpClient createHttpClient(boolean ignoreCert) {
        try {
            RequestConfig requestConfig = RequestConfig.custom()
                    .setCookieSpec(CookieSpecs.STANDARD)
                    .build();

            CloseableHttpClient client = null;

            if (ignoreCert) {
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(new KeyManager[0], new TrustManager[]{new NoopTrustManager()}, new SecureRandom());
                SSLContext.setDefault(sslContext);

                SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(
                        sslContext, NoopHostnameVerifier.INSTANCE);

                client = HttpClients.custom()
                        .disableRedirectHandling()
                        .setDefaultRequestConfig(requestConfig)
                        .setSSLSocketFactory(sslSocketFactory)
                        .build();
            } else {
                client = HttpClientBuilder.create()
                        .disableRedirectHandling().disableAutomaticRetries()
                        .setDefaultRequestConfig(requestConfig)
                        .build();
            }

            return client;
        } catch (Throwable ex) {
            throw new RuntimeException(String.format(
                    "Failed to create http client (ignoreCert = %s)",
                    ignoreCert), ex);
        }
    }

    private    void setHeaders(HttpRequestBase httpBase) {
        //String token="";
        String token="eyJhbGciOiJSUzI1NiIsImtpZCI6InB1YmxpYzpkZWU4MGRkNC0wODE1LTQ3ZTItOGZmMy0wMGRhOWZkNWY5NzgiLCJ0eXAiOiJKV1QifQ.eyJhdWQiOltdLCJjbGllbnRfaWQiOiJTUEEtRVMtQVBQLVNQQS1FUy1BcHAtZmY5NWU0IiwiZXhwIjoxNjM0NDc5MzUwLCJleHQiOnsiY3VzdG9tZXIiOiJob25leXdlbGwifSwiaWF0IjoxNjM0NDc1NzQ5LCJpc3MiOiJodHRwczovL2ZvcmdlLWlkZW50aXR5LXFhLmRldi5zcGVjLmhvbmV5d2VsbC5jb20vIiwianRpIjoiODQzMjI2ZjctMGVlMS00MDUwLWJhM2ItNzllYjBlOGIwNjFmIiwibmJmIjoxNjM0NDc1NzQ5LCJzY3AiOlsib3BlbmlkIiwib2ZmbGluZSIsImdyb3VwcyIsImVtYWlsIl0sInN1YiI6ImZkZWYwY2Q5LTU5ZGYtODRjOS1lZTU2LTUwMjNmZWMwZGFiZCJ9.S0J0e9HNkf2yBpE4h5Skp-QW4UaZsToFuVdKf4HaaS94XzMNauNtmZFBgzTDa4vvwxWhx_ZKcy4qvOhiZahwuwTYd3OcYqWDf2o9knL54C3rutP22VAiBZXI1e3cJxxl63mv0_obwTYAXod4wOt32Mf2iFlvOQ2XFR4uQk3tXwUhfdgv5buVgHZuaAHTWmj-0mr7zowsJXWO16LRRcP24LZOYMmHeQnrB0QneESdobz_79FUEKR46y48_LSet4EQwkdE2tyAPu7ppKBquoljn-oiTkvDhXJ3ouE3-cbFhhRjyKTJGdDgW2xPEgclO-BYvnE-HV-n_kj0fEt0NCeSj5Ke_ulbgZdR6reHIhffNRW16q7A8O4c6xVeyhiiFAaRD-4bghEcWNgAb32XaISzpD6J8cnmVymiQI-bXb8Su470E5Z73rB78v9Zo_m6NVXvG_17ww-Rv_eNlwdkUP4dv28qX3B7_nkToDOjIYIIIgEtd-K0LYmYYwlZkWNF4vuTT0KuL8XB6vIN9IrTAlcljy7lQYfycCQcKFVBriaREgkz79BhOsGBLJ5vX320AbxAN4xPbVTy0ar9hUk9xQ0MH-gmag2TWB-PFG5bIJqjIc9ZYTKf7uwpWumP6aMblRk_-wIX-ZCFswmMkQ09V0cegjRSXuipzKZoLVPPgBQMzi8";
        httpBase.setHeader( "Authorization","Bearer " +token);
        httpBase.setHeader("Accept","application/json");
    }


    public String call(HttpRequestOptions options) {

        HttpEntityEnclosingRequestBase request = null;
        System.out.println("Call ");

        try {
            //set verbs and create client object
            setRequestType(options);
            // TODO: Take B timeout value and throw an exception in case the HTTP server doesn't respond in due time
            this.response = this.httpClient.execute(this.httpRequest);

            HttpEntity entity = this.response.getEntity();
            System.out.println("Content posted to: " + this.httpRequest.toString());

            if (response.getStatusLine().getStatusCode() == Response.Status.OK.getStatusCode()) {
                System.out.println("Status ok");
            }
            //  System.out.println("Entity  " +  EntityUtils.toString(response.getEntity(), "UTF-8"));
            getResponseHeaders();
            return  EntityUtils.toString( entity);


            //String json=prettyPrint(EntityUtils.toString(response.getEntity(), "UTF-8"));
            //return json;
        } catch (UnsupportedEncodingException e) {
            throw new WebDriverException(ERROR_MESSAGE);
        } catch (ClientProtocolException e) {
            Log.log("EXCEPTION", ExceptionUtils.getStackTrace(e), false);
            throw new WebDriverException(ERROR_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
            Log.log("IO EXCEPTION", ExceptionUtils.getStackTrace(e), false);
            throw new WebDriverException(ERROR_MESSAGE);
        } finally {
        }
    }

    protected String getURL() {
        return this.url;
    }

    public int getResponseStatusCode() {
        return this.response.getStatusLine().getStatusCode();
    }

    public InputStream getResponseAsStream() throws IOException {
        return this.response.getEntity().getContent();
    }

    public Map<String, String> getResponseHeaders() {
        Header[] headers = this.response.getAllHeaders();
        Map<String, String> headersMap = new HashMap<String, String>();
        for (Header header : headers) {
            System.out.println("Header Name :: "+header.getName()+ " Value :: " +header.getValue());
            headersMap.put(header.getName(), header.getValue());
        }
        return headersMap;
    }

    public MethodType getHttpVerb() {
        return this.httpVerb;
    }


    public String getResponseAsString() {
        HttpEntity responseEntity = this.response.getEntity();

        if (responseEntity != null) {
            try {
                InputStream contentStream = responseEntity.getContent();

                if (contentStream != null) {
                    return IOUtils.toString(contentStream, "UTF-8");
                } else {
                    return "";
                }
            } catch (Exception ex) {
                throw new RuntimeException(String.format("Failed to get the response content for HTTP request %s %s",
                        this.httpVerb,
                        this.url), ex);
            }
        } else {
            throw new RuntimeException(String.format("Failed to get B response for HTTP request %s %s",
                    this.httpVerb,
                    this.url));
        }
    }

    public String getFirstHeader(String headerName) {
        Header header = this.response.getFirstHeader(headerName);
        return header != null ? header.getValue() : null;
    }

    public void setContent(String content, String contentType) {
        if (content == null) {
            content = "";
        }

        //TODO: Improve the validation logic
        if (contentType.indexOf('/') <= 0) {
            throw new RuntimeException(String.format("Content type \"%s\" is not B valid MIME type", contentType));
        }

        if (HttpEntityEnclosingRequestBase.class.isInstance(httpRequest)) {
            try {
                StringEntity requestEntity = new StringEntity(content, Charset.forName("UTF-8"));
                ((HttpEntityEnclosingRequestBase) this.httpRequest).setEntity(requestEntity);
                this.httpRequest.setHeader("Content-Type", contentType);
            } catch (Exception ex) {
                throw new RuntimeException("Failed to set HTTP request content", ex);
            }
        }
    }

    public void setHeader(String headerName, String headerValue) {
        this.httpRequest.setHeader(headerName, headerValue);
    }

    public void setProxy(String proxyServer) {
        String proxy = null;
        String proxyPort = null;
        Pattern pattern = Pattern.compile("(?<proxy>.+?)(:(?<port>.+))?");
        Matcher matcher = pattern.matcher(proxyServer.trim());
        if (matcher.matches()) {
            proxy = matcher.group("proxy");
            proxyPort = matcher.group("port");
        } else {
            throw new RuntimeException(String.format("Invalid proxy server:", proxyServer));
        }

        HttpHost proxyHost;
        if (proxyPort != null) {
            proxyHost = new HttpHost(proxy, Integer.valueOf(proxyPort));
        } else {
            proxyHost = new HttpHost(proxy);
        }
        RequestConfig oldConfig = this.httpRequest.getConfig();
        RequestConfig.Builder configBuilder = null;

        if (oldConfig != null) {
            configBuilder = RequestConfig.copy(oldConfig);
        } else {
            configBuilder = RequestConfig.custom().setProxy(proxyHost);
        }

        this.httpRequest.setConfig(configBuilder.build());
    }
}
