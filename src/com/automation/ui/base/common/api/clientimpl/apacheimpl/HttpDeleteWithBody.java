package com.automation.ui.base.common.api.clientimpl.apacheimpl;


import org.apache.http.client.methods.HttpPost;

import java.net.URI;

public class HttpDeleteWithBody extends HttpPost {

    HttpDeleteWithBody(String uri) {
        super(uri);
    }

    HttpDeleteWithBody(URI uri) {
        super(uri);
    }

    @Override
    public String getMethod() {
        return "DELETE";
    }
}
