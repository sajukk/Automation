package com.automation.ui.base.common.api.clientimpl.apacheimpl;

import org.apache.http.HttpResponse;

public class RestClientException extends Exception {
 
    private final HttpResponse response;

    public RestClientException(String message, HttpResponse response) {
	super(message);
	this.response = response;
    }

    public RestClientException(Exception e, HttpResponse response) {
	super(e);
	this.response = response;
    }

    public HttpResponse response() {
	return response;
    }
}