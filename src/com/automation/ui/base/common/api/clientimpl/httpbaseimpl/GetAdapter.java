package com.automation.ui.base.common.api.clientimpl.httpbaseimpl;

import com.automation.ui.base.common.api.adapter.AbstractAdapter;
import com.automation.ui.base.common.api.util.ContentType;
import com.automation.ui.base.common.api.util.MethodType;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.apache.http.message.BasicNameValuePair;

//import org.apache.http.client.methods.HttpGet;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.HashMap;

public class GetAdapter extends AbstractAdapter  {
    private String name;

    //   HttpGet request = new HttpGet("http://lifecharger.org/3-tips-for-a-better-life/");

    protected GetAdapter(GetBuilder<?, ?> builder) {
        super(builder);
        this.name = builder.name;

    }

    public static GetBuilder<?, ?> builder() {
        return new DefaultGetBuilder();
    }




    public String getName() {
        return name;
    }
/*
    @Override
    public <T> T execute(Class<T> responseClass) {
        Gson jsonParser = new Gson();
        // final String endpoint = getEndPoint() + getMethod();
        final String endpoint = getEndPoint() ;
        String body = jsonParser.toJson(getObject());
        HttpURLConnection request = null;
        try {
            request = getRequest(endpoint);
            HashMap<String,String> hm=setHeaders();
            Iterator<String> itr = hm.keySet().iterator();
            while (itr.hasNext()) {
                String key = itr.next();
                String value = hm.get(key);
                request.setRequestProperty( key, value);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        String response = null;
        try {
            response = makeRawRequest(request, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Request  :: " + prettyPrint(body) + "\n");
        System.out.println("Response :: " + prettyPrint(response) + "\n");
        return jsonParser.fromJson(response, responseClass);
    }
*/
    private HttpURLConnection getRequest(String command) throws IOException {
        URL obj = new URL(command);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod(MethodType.GET.getMethodType());
        con.setRequestProperty("Content-Type", ContentType.JSON.getContentType());
        con.setAllowUserInteraction(true);
        return con;
    }

    public static abstract class GetBuilder<S extends GetAdapter, B
            extends GetBuilder<S, B>> extends AbstractBuilder<S, B> {
        private String name;

        @SuppressWarnings("unchecked")
        public B name(String name) {
            this.name = name;
            return (B) this;
        }

    }

    private static class DefaultGetBuilder extends
            GetBuilder<GetAdapter, DefaultGetBuilder> {
        @Override
        public GetAdapter build() {
            return new GetAdapter(this);
        }
    }

    public static void main (String str[])
    {
        GetAdapter ga=new GetAdapter(GetAdapter.builder()

                .setEndPoint("https://enabledservicesqa.honeywell.com/gateway/AddUserEmailMapping/c026d7fa-d772-4511-ac8d-fbe9465f8702/dddddd@honeywell.com")
                .setContentType(ContentType.JSON)
                .setHeaders(setHeaders())
                .setMethodName(MethodType.GET.getMethodType())

        );
        //Gson gs=ga.execute(Gson.class);
       // gs.toString();
    }

    private static HashMap<String,String> setHeaders() {
        String token="eyJhbGciOiJSUzI1NiIsImtpZCI6InB1YmxpYzpkZWU4MGRkNC0wODE1LTQ3ZTItOGZmMy0wMGRhOWZkNWY5NzgiLCJ0eXAiOiJKV1QifQ.eyJhdWQiOltdLCJjbGllbnRfaWQiOiJTUEEtRVMtQVBQLVNQQS1FUy1BcHAtZmY5NWU0IiwiZXhwIjoxNjM0Mzg1NjAwLCJleHQiOnsiY3VzdG9tZXIiOiJob25leXdlbGwifSwiaWF0IjoxNjM0MzgxOTk5LCJpc3MiOiJodHRwczovL2ZvcmdlLWlkZW50aXR5LXFhLmRldi5zcGVjLmhvbmV5d2VsbC5jb20vIiwianRpIjoiNjcxZjQ4MjYtMjVlNi00MjIxLTgwNGItMjEzY2Y0ZDAzMzViIiwibmJmIjoxNjM0MzgxOTk5LCJzY3AiOlsib3BlbmlkIiwib2ZmbGluZSIsImdyb3VwcyIsImVtYWlsIl0sInN1YiI6ImZkZWYwY2Q5LTU5ZGYtODRjOS1lZTU2LTUwMjNmZWMwZGFiZCJ9.OKSzIBL8giaDDgZhu4SPEMpzKfJjedvWXF39xcq0tAEuYSF-Ud9TNhLacZXLncFJnQ5DSpnY2aqi_zxqQkig5I75IIyjhBLLE_n27u-e_OZ6QrrgAA6bDDkaIeWaFl5Ds4766bhWHG6-wN-5TUwFW1akJDxHXorskv6WzI55re1JSi_lmB_1fdqi2ybQkiu1SfjJY-e8epqBr-aUY4Do59aUYa5f3DaFZsDGEy5nNk5HXxGGllCfhcBG80_6VkReOraGRhuKXbZA--Vg3HNhFfhAMW0ZkmkZvpsAyEyfZyLfy38NMJ3XoiEVFfuqz4sZy7ykqfbua1HANyyn8fqmh5nstAu_yYeF2TSfC3YsuZn1TO8i8pOOAA81jgAzBHULCXeTLlJEWFFBO1OZeD6Kiec88HmoW0rrg-_eVDqGzaPr6gMchLvKqbLWhjgZLXM9BJU_LQ7B6VuEDfkIunjELY3xDHHAREQCtjXoGKcIHj0yKpNp2BJRqHYzjjp5qLIC214jqa52WPApeRX29Q5ECTZaPYd9cFVq4fLmepMHGKltHN7VdSXyeKbiGDk-4k06pXhCUCTOFRmJd0MwpYZl2f7Y-UcEaCDxfJZ2rG5MO91Jq2v7oeUccoE4MwibTHmPBxl6Xj5XhjYe76wd1BtSR3erYP-tsgGgQYkBO8Val1U";
        HashMap params = new HashMap<>();
        params.put( "Authorization","Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6InB1YmxpYzpkZWU4MGRkNC0wODE1LTQ3ZTItOGZmMy0wMGRhOWZkNWY5NzgiLCJ0eXAiOiJKV1QifQ.eyJhdWQiOltdLCJjbGllbnRfaWQiOiJTUEEtRVMtQVBQLVNQQS1FUy1BcHAtZmY5NWU0IiwiZXhwIjoxNjM0Mzg1NjAwLCJleHQiOnsiY3VzdG9tZXIiOiJob25leXdlbGwifSwiaWF0IjoxNjM0MzgxOTk5LCJpc3MiOiJodHRwczovL2ZvcmdlLWlkZW50aXR5LXFhLmRldi5zcGVjLmhvbmV5d2VsbC5jb20vIiwianRpIjoiNjcxZjQ4MjYtMjVlNi00MjIxLTgwNGItMjEzY2Y0ZDAzMzViIiwibmJmIjoxNjM0MzgxOTk5LCJzY3AiOlsib3BlbmlkIiwib2ZmbGluZSIsImdyb3VwcyIsImVtYWlsIl0sInN1YiI6ImZkZWYwY2Q5LTU5ZGYtODRjOS1lZTU2LTUwMjNmZWMwZGFiZCJ9.OKSzIBL8giaDDgZhu4SPEMpzKfJjedvWXF39xcq0tAEuYSF-Ud9TNhLacZXLncFJnQ5DSpnY2aqi_zxqQkig5I75IIyjhBLLE_n27u-e_OZ6QrrgAA6bDDkaIeWaFl5Ds4766bhWHG6-wN-5TUwFW1akJDxHXorskv6WzI55re1JSi_lmB_1fdqi2ybQkiu1SfjJY-e8epqBr-aUY4Do59aUYa5f3DaFZsDGEy5nNk5HXxGGllCfhcBG80_6VkReOraGRhuKXbZA--Vg3HNhFfhAMW0ZkmkZvpsAyEyfZyLfy38NMJ3XoiEVFfuqz4sZy7ykqfbua1HANyyn8fqmh5nstAu_yYeF2TSfC3YsuZn1TO8i8pOOAA81jgAzBHULCXeTLlJEWFFBO1OZeD6Kiec88HmoW0rrg-_eVDqGzaPr6gMchLvKqbLWhjgZLXM9BJU_LQ7B6VuEDfkIunjELY3xDHHAREQCtjXoGKcIHj0yKpNp2BJRqHYzjjp5qLIC214jqa52WPApeRX29Q5ECTZaPYd9cFVq4fLmepMHGKltHN7VdSXyeKbiGDk-4k06pXhCUCTOFRmJd0MwpYZl2f7Y-UcEaCDxfJZ2rG5MO91Jq2v7oeUccoE4MwibTHmPBxl6Xj5XhjYe76wd1BtSR3erYP-tsgGgQYkBO8Val1U");
        params.put( "Accept","application/json");
        return params;

    }


    private static HashMap<String,String> setQueryParams() {
        HashMap params = new HashMap<>();
        params.put( "username", "user");
        params.put( "password", "pass");
        return params;

    }
}
