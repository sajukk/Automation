package com.automation.ui.base.common.api.clientimpl.httpbaseimpl;

import com.automation.ui.base.common.api.adapter.AbstractAdapter;
import com.automation.ui.base.common.api.util.ContentType;
import com.automation.ui.base.common.api.util.MethodType;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostAdapter extends AbstractAdapter   {
    private String name;

    protected PostAdapter(GetBuilder<?, ?> builder) {
        super(builder);
        this.name = builder.name;

    }


    public static GetBuilder<?, ?> builder() {
        return new DefaultGetBuilder();
    }


    public String getName() {
        return name;
    }



    private HttpURLConnection postRequest(String command, String body) throws IOException {
        HttpURLConnection con = null;
        try {
            URL obj = new URL(command);
            con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(MethodType.POST.getMethodType());
            con.setRequestProperty("Content-Type", ContentType.JSON.getContentType());
            con.setDoOutput(true);
            OutputStreamWriter osw = new OutputStreamWriter(con.getOutputStream());
            osw.write(body);
            osw.flush();
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return con;
    }

    public static abstract class GetBuilder<S extends PostAdapter, B extends GetBuilder<S, B>> extends AbstractBuilder<S, B> {
        private String name;

        @SuppressWarnings("unchecked")
        public B name(String name) {
            this.name = name;
            return (B) this;
        }

    }

    private static class DefaultGetBuilder extends GetBuilder<PostAdapter, DefaultGetBuilder> {
        @Override
        public PostAdapter build() {
            return new PostAdapter(this);
        }
    }
}
