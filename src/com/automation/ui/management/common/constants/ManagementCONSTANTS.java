package com.automation.ui.management.common.constants;

/**
 * @author
 */


public interface ManagementCONSTANTS {


    String EXCEPTIONPATH = "resources/properties/exception.properties";
    String ASSERTPATH = "i18n.assertdata";
    String LOGINJSONDATAPATH = "resources/logindataselenium.json";


}
