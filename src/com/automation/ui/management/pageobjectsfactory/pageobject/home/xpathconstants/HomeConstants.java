package com.automation.ui.management.pageobjectsfactory.pageobject.home.xpathconstants;

/**
 * @author
 */
public interface HomeConstants {
    public static String SEARCHINDUSTRY = "//select[@id='industry']";
    public static String SEARCHMARKETNEED = "//input[@id='businessProblem']";
    public static String SEARCHLEADMANAGEMENT = "(//div[contains(.,'lead management')])[1]";
    public static String SEARCHBUSINESSGOAL = "//select[contains(@id,'goal_select')]";
    public static String CONTINUE = "(//a[contains(.,'Continue')])[1]";

    public static String CCLINK = "//a[@class='cc-link']";
    public static String CCDISMISS = "//a[@class='cc-btn cc-dismiss']";
    public static String ACCPRODUCT = "//a[@href='#scc-product']";
    public static String BLOGLINK = "//a[@href='https://blog.management.com']";
    public static String SECABOUT = "//a[@href='#sec-about']";
    public static String SIGNUP = "//a[@href='#signupmodalonClick']";
    public static String PRODUCT = "//a[@href='#prd']";
    public static String ABOUT = "//a[@href='#abt-us']";
    public static String CONTACT = "//a[@href='https://blog.management.com/contact/']";
    public static String PRIVACY = "//a[@href='/privacy']";
    public static String TERMS = "//a[@href='/terms']";
    public static String AIRLINKEDIN = "//a[@href='https://www.linkedin.com/company/airobot/']";
    public static String AIRTWITTER = "//a[@href='https://twitter.com/airobot']";
    public static String MAILTO = "//a[@href='mailto:info@ai-robot.com']";
    public static String LINKEDINBUTTON = "//a[@class='linkedin-btn d-block my-3 m-auto']";
    public static String CONTASGUEST = "//a[@class='cp-continue-gest m-auto my-3']";
    public static String REGISTER = "//a[@href='/register']";
    public static String CLOSEMODEL = "//button[@class='close close-modal btn']";
    public static String CLOSE = "//button[@class='close']";

}