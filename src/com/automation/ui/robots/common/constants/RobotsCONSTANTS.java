package com.automation.ui.robots.common.constants;

/**
 * @author
 */


public interface RobotsCONSTANTS {


    String EXCEPTIONPATH = "resources/properties/exception.properties";
    String ASSERTPATH = "i18n.assertdata";
    String LOGINJSONDATAPATH = "resources/logindataselenium.json";


}
