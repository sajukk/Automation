package com.automation.ui.robots.pageobjectsfactory.pageobject.context.dataprovider;
/**
 * @author
 */

import com.automation.ui.robots.pageobjectsfactory.pageobject.base.Goals;
import com.automation.ui.robots.pageobjectsfactory.pageobject.base.Industry;
import com.automation.ui.robots.pageobjectsfactory.pageobject.context.vo.ContextVO;
import org.testng.annotations.DataProvider;


public class ContextDataProvider {

    public ContextDataProvider() {

    }



    @DataProvider(name = "initContextStarter")
    public static final Object[][] getContextStarter() {

        ContextVO initContextVo1= new ContextVO();

        return new Object[][]
                {
                        {initContextVo1}
                };
    }



}
