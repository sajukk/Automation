package com.automation.ui.robots.pageobjectsfactory.pageobject.home.xpathconstants;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author
 */
public interface HomeConstants {
    public static String SEARCH = "//i[contains(@class,'fa fa-search')]";
    public static String LOGIN = "//a[@id='login']";
    public static String SIGNUP = "//a[@id='signup']";
    public static String SIGNIN = "//a[@id='signup']";
    public static String AIRLOGO = "//a[@id='login']";
    public static String SITETERM = "//a[@id='login']";
    public static String CONTACT = "//a[@id='login']";
    public static String PRIVACY = "//a[@id='login']";
    public static String LINKEDINBUTTON = "//a[@id='login']";
    public static String FBBUTTON = "//a[@id='login']";
    public static String TWBUTTON = "//a[@id='login']";

}